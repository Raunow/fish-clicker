﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Fish_Clicker
{
    class Fyke : GameObject
    {
        #region Fields
        GameWorld gw;
        //knapper: (true statement fra knap vil upgradere 1 gang eller købe 1)
        private bool buying;
        private bool upgrading;

        //do not touch these:
        private int level, upgradePrice, produce, owned, price;
        #endregion

        #region Properties
        public int Price
        {
            get
            {
                return price;
            }
        }

        public int UpgradePrice
        {
            get
            {
                return upgradePrice;
            }
        }

        public bool Buying
        {
            set
            {
                buying = value;
            }
        }

        public bool Upgrading
        {
            set
            {
                upgrading = value;
            }
        }

        public int Level
        {
            set
            {
                level = value;
            }
            get
            {
                return level;
            }
        }

        public int Owned
        {
            set
            {
                owned = value;
            }
            get
            {
                return owned;
            }
        }
        #endregion

        public Fyke(GameWorld gw)
        {
            this.gw = gw;

            Initialize();
        }

        #region Methods
        public void Initialize()
        {
            upgradePrice = 1100;
            price = 1100;
            produce = 0;
            level = 0;
            owned = 0;
            buying = false;
            upgrading = false;
        }

        public override void LoadContent(ContentManager content)
        {

        }

        public override void Update(GameTime gameTime)
        {
            gw.FykeProd = produce;
            //Raises the price by 10% for each unit
            if (buying && gw.FishCount >= price)
            {
                produce = 8;
                gw.FishCount -= price;
                owned++;
            }

            //multiplies the price by 2 per level
            if (upgrading && gw.FishCount >= upgradePrice * (int)Math.Pow(2, (double)level))
            {
                gw.FishCount -= upgradePrice;
                upgradePrice *= 5;
                level++;
            }
            price = 1100;
            for (int i = 0; i < owned + 1; i++)
            {
                price = (price + (price / 5));
            }
            produce = 8 * owned * (int)Math.Pow(2, (double)level);
            upgrading = false;
            buying = false;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

        }
        #endregion

    }
}