﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Fish_Clicker
{
    class Fish : GameObject
    {
        #region Fields
        GameWorld gw;
        //knapper: (true statement fra knap vil upgradere 1 gang eller købe 1)
        private bool upgrading;

        //do not touch these:
        private int level, upgradePrice, produce;
        #endregion

        #region Properties
        public int UpgradePrice
        {
            get
            {
                return upgradePrice;
            }
        }

        public bool Upgrading
        {
            set
            {
                upgrading = value;
            }
        }

        public int Level
        {
            set
            {
                level = value;
            }
            get
            {
                return level;
            }
        }
        #endregion

        public Fish(GameWorld gw)
        {
            this.gw = gw;

            Initialize();
        }

        #region Methods
        public void Initialize()
        {
            upgradePrice = 200;
            produce = 0;
            level = 0;
            upgrading = false;
        }

        public override void LoadContent(ContentManager content)
        {

        }

        public override void Update(GameTime gameTime)
        {
            gw.FishProd = produce;
            //multiplies the price by 5 per level and increases gain 2 times per level
            if (upgrading && gw.FishCount >= upgradePrice * (int)Math.Pow(2, (double)level))
            {
                gw.FishCount -= upgradePrice;
                upgradePrice *= 5;
                level++;
                upgrading = false;
            }

            produce = (int)Math.Pow(2, (double)level);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

        }
        #endregion

    }
}
