﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace LinQ_test
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] autoclickers = { "Fishingrod", "Fisherman", "Fyke", "Row boat", "Cutter"};
            var queryResults =
            from n in autoclickers
            where n.StartsWith("F")
            select n;
            Console.WriteLine("Autoclickers with starting with F:");
            foreach (var item in queryResults)
            { Console.WriteLine(item); }
            Console.ReadLine();
        }
    }
}