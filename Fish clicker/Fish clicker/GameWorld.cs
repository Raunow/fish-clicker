﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Fish_Clicker
{
    public class GameWorld : Game
    {
        #region Fields
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<GameObject> gameObjects;
        Texture2D backGround;
        SpriteFont spriteFont;
        Vector2 totalPos, screenBoundaries, standardViewport, ratio;

        int totalClicks;
        const float delay = 100; // milliseconds
        float remainingDelay = delay, totalFish, fishProd, fishingrodProd, fishermanProd, fykeProd, rowBoatProd, cutterProd, fishCount, totalProd;
        #endregion

        #region properties
        public int TotalClicks
        {
            get
            {
                return totalClicks;
            }
            set
            {
                totalClicks = value;
            }
        }

        public Vector2 Ratio
        {
            get
            {
                return ratio;
            }
        }

        public float TotalFish
        {
            get
            {
                return totalFish;
            }
            set
            {
                totalFish = value;
            }
        }

        public float FishCount
        {
            get
            {
                return fishCount;
            }
            set
            {
                fishCount = value;
            }
        }

        public float FishProd
        {
            set
            {
                fishProd = value;
                TotalProduce();
            }
        }

        public float FishingrodProd
        {
            set
            {
                fishingrodProd = value;
                TotalProduce();
            }
        }

        public float FishermanProd
        {
            set
            {
                fishermanProd = value;
                TotalProduce();
            }
        }

        public float FykeProd
        {
            set
            {
                fykeProd = value;
                TotalProduce();
            }
        }

        public float RowBoatProd
        {
            set
            {
                rowBoatProd = value;
                TotalProduce();
            }
        }

        public float CutterProd
        {
            set
            {
                cutterProd = value;
                TotalProduce();
            }
        }
        #endregion

        #region Constructor
        public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.IsFullScreen = true;
            screenBoundaries.X = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            screenBoundaries.Y = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            standardViewport = new Vector2(1920, 1080);
            ratio.Y = (int)screenBoundaries.Y / (int)standardViewport.Y;
            ratio.X = (int)screenBoundaries.X / (int)standardViewport.X;
            graphics.PreferredBackBufferHeight = (int)ratio.Y * (int)screenBoundaries.Y;
            graphics.PreferredBackBufferWidth = (int)ratio.X * (int)screenBoundaries.X;

            Content.RootDirectory = "Content";
        }
        #endregion

        #region Mehods
        protected override void Initialize()
        {
            gameObjects = new List<GameObject>();
            gameObjects.Add(new Fish(this));
            gameObjects.Add(new Fishingrod(this));
            gameObjects.Add(new Fisherman(this));
            gameObjects.Add(new Fyke(this));
            gameObjects.Add(new RowBoat(this));
            gameObjects.Add(new Cutter(this));
            gameObjects.Add(new Table(this, (Fish)gameObjects[0], (Fishingrod)gameObjects[1], (Fisherman)gameObjects[2], (Fyke)gameObjects[3], (RowBoat)gameObjects[4], (Cutter)gameObjects[5]));
            gameObjects.Add(new Button(this, (Fish)gameObjects[0], (Fishingrod)gameObjects[1], (Fisherman)gameObjects[2], (Fyke)gameObjects[3], (RowBoat)gameObjects[4], (Cutter)gameObjects[5], (Table)gameObjects[6]));

            IsMouseVisible = true;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            backGround = Content.Load<Texture2D>("Background");
            spriteFont = Content.Load<SpriteFont>("spriteFont");

            foreach (GameObject item in gameObjects)
            {
                item.LoadContent(Content);
            }
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                GameExit();
            foreach (GameObject item in gameObjects)
            {
                item.Update(gameTime);
            }

            //Timer for adding fish every tenth of a second
            #region Timer
            var timer = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            remainingDelay -= timer;

            if (remainingDelay <= 0)
            {
                remainingDelay = delay;
                //Code that executes when the timer ticks

                fishCount += (totalProd / 10);
                totalFish += totalProd;
            }
            #endregion

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.ForestGreen);
            spriteBatch.Begin();

            spriteBatch.Draw(backGround, Vector2.Zero, Color.White);

            foreach (GameObject item in gameObjects)
            {
                item.Draw(spriteBatch);
            }
            totalPos = new Vector2(170, 330);
            spriteBatch.DrawString(spriteFont, "Fish: " + Math.Round(fishCount, 1), totalPos, Color.White);
            totalPos.Y += 15;
            spriteBatch.DrawString(spriteFont, "Fish/sec: " + Math.Round(totalProd, 1), totalPos, Color.White);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void GameExit()
        {
            (gameObjects[6] as Table).Save();
            (gameObjects[6] as Table).Close();
            Exit();
        }

        public void TotalProduce()
        {
            if ((gameObjects[0] as Fish).Level > 0)
            {
                totalProd = fishProd * (fishingrodProd + fishermanProd + fykeProd + rowBoatProd + cutterProd);
            }
            else
            {
                totalProd = fishingrodProd + fishermanProd + fykeProd + rowBoatProd + cutterProd;
            }
        }
        #endregion
    }
}
