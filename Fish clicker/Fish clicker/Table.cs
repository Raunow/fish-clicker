﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Fish_Clicker
{
    class Table : GameObject
    {
		#region Fields
		private int test1, test2, test3;
        private string filePath, sql;
        SQLiteConnection datadb;
        SQLiteCommand command;
        SQLiteDataReader reader;
        GameWorld gw;
        Fish fh;
        Fishingrod fr;
        Fisherman fm;
        Fyke fk;
        RowBoat rb;
        Cutter cr;
        #endregion

        #region Constructors
        public Table(GameWorld gw, Fish fh, Fishingrod fr, Fisherman fm, Fyke fk, RowBoat rb, Cutter cr)
        {
            this.gw = gw;
            this.fh = fh;
            this.fr = fr;
            this.fm = fm;
            this.fk = fk;
            this.rb = rb;
            this.cr = cr;

            //Concattenates the programs location and filename.
            filePath = Directory.GetCurrentDirectory() + @"\database.db";
            //checks wheher the file exists. If it doesnt exist it will create the database
            datadb = new SQLiteConnection("Data Source=database.db;Version=3;");
            datadb.Open();

            if (!File.Exists(filePath))
            {
                SQLiteConnection.CreateFile("database.db");
            }

			UpdateTables("create table if not exists upgrades (id integer Primary Key NOT NULL, fishingrod int NOT NULL, fisherman int NOT NULL, fyke int NOT NULL, rowboat int NOT NULL, cutter int NOT NULL, fish int NOT NULL);");
			UpdateTables("create table if not exists stats (id integer Primary Key NOT NULL, fish int NOT NULL, totalfish int NOT NULL, totalclicks int NOT NULL);");
			UpdateTables("create table if not exists autoclickers (id integer Primary Key NOT NULL, fishingrod int NOT NULL, fisherman int NOT NULL, fyke int NOT NULL, rowboat int NOT NULL, cutter int NOT NULL);");

			sql = "select * from stats";
			command = new SQLiteCommand(sql, datadb);
			reader = command.ExecuteReader(); //runs the given command with the reader()
			while (reader.Read()) //prints every from the table
			{
				test1 = (int)reader["fish"];
				test2 = (int)reader["totalfish"];
				test3 = (int)reader["totalclicks"];
			}

			if (test1 == 0)
			{
				UpdateTables("insert into upgrades (id, fishingrod, fisherman, fyke, rowboat, cutter, fish) values ( null, 0, 0, 0, 0, 0, 0);");
				UpdateTables("insert into stats (id, fish, totalfish, totalClicks) values ( null, 0, 0, 0);");
				UpdateTables("insert into autoclickers (id, fishingrod, fisherman, fyke, rowboat, cutter) values ( null, 0, 0, 0, 0, 0);");
			}
		}
        #endregion

        #region Methods
        //Saves the necessary information from the RAM to the tables
        public void Save()
        {
            UpdateTables($"update upgrades set fishingrod={fr.Level}");
            UpdateTables($"update upgrades set fisherman={fm.Level}");
            UpdateTables($"update upgrades set fyke={fk.Level}");
            UpdateTables($"update upgrades set rowboat={rb.Level}");
            UpdateTables($"update upgrades set cutter={cr.Level}");
            UpdateTables($"update upgrades set fish={fh.Level}");
            
            UpdateTables($"update stats set fish={gw.FishCount}");
            UpdateTables($"update stats set totalfish={gw.TotalFish}");
            UpdateTables($"update stats set totalclicks={gw.TotalClicks}");

            UpdateTables($"update autoclickers set fishingrod={fr.Owned}");
            UpdateTables($"update autoclickers set fisherman={fm.Owned}");
            UpdateTables($"update autoclickers set fyke={fk.Owned}");
            UpdateTables($"update autoclickers set rowboat={rb.Owned}");
            UpdateTables($"update autoclickers set cutter={cr.Owned}");
        }

        //reads the tables and sets the game values
        public void Load()
        {
            sql = "select * from upgrades";
            command = new SQLiteCommand(sql, datadb);
            reader = command.ExecuteReader(); //runs the given command with the reader()
            while (reader.Read()) //prints every from the table
            {
                fr.Level = (int)reader["fishingrod"];
                fm.Level = (int)reader["fisherman"];
                fk.Level = (int)reader["fyke"];
                rb.Level = (int)reader["rowboat"];
                cr.Level = (int)reader["cutter"];
            }

            sql = "select * from stats";
            command = new SQLiteCommand(sql, datadb);
            reader = command.ExecuteReader(); //runs the given command with the reader()
            while (reader.Read()) //prints every from the table
            {
                gw.FishCount = (int)reader["fish"];
                gw.TotalFish = (int)reader["totalfish"];
                gw.TotalClicks = (int)reader["totalclicks"];
            }

            sql = "select * from autoclickers";
            command = new SQLiteCommand(sql, datadb);
            reader = command.ExecuteReader(); //runs the given command with the reader()
            while (reader.Read()) //prints every from the table
            {
                fr.Owned = (int)reader["fishingrod"];
                fm.Owned = (int)reader["fisherman"];
                fk.Owned = (int)reader["fyke"];
                rb.Owned = (int)reader["rowboat"];
                cr.Owned = (int)reader["cutter"];
            }
        }

        //Delete but also "Refreshes" the database
        public void Delete()
        {
            UpdateTables("delete from upgrades;");
            UpdateTables("delete from stats;");
            UpdateTables("delete from autoclickers;");

            UpdateTables("insert into upgrades (id, fishingrod, fisherman, fyke, rowboat, cutter, fish) values (null, 0, 0, 0, 0, 0, 0);");
            UpdateTables("insert into stats (id, fish, totalfish, totalclicks) values (null, 0, 0, 0);");
            UpdateTables("insert into autoclickers (id, fishingrod, fisherman, fyke, rowboat, cutter) values (null, 0, 0, 0, 0, 0);");
        }

        public void Close()
        {
            datadb.Close();
        }
        //Method for executing commands more fluidly
        public void UpdateTables(string sqlInput)
        {
            command = new SQLiteCommand(sqlInput, datadb);
            command.ExecuteNonQuery();
        }

        public override void LoadContent(ContentManager content)
        {
        }
        public override void Update(GameTime gameTime)
        {
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
        }
        #endregion

    }
}
/*
 * Code for another time.
            jaggedSaveCommands[0] = new string[]
            {

                $"update upgrades set fishingrod={fr.Level}",
                $"update upgrades set fisherman={fm.Level}",
                $"update upgrades set fyke={fk.Level}",
                $"update upgrades set rowboat={rb.Level}",
                $"update upgrades set cutter={cr.Level}",
                $"update upgrades set fish={fh.Level}"
            };

            jaggedSaveCommands[1] = new string[]
            {
                $"update stats set fish={gw.FishCount}",
                $"update stats set totalfish={gw.TotalFish}",
                $"update stats set totalclicks={gw.TotalClicks}"
                
            };

            jaggedSaveCommands[2] = new string[]
            {
                $"update autoclickers set fishingrod={fr.Owned}",
                $"update autoclickers set fisherman={fm.Owned}",
                $"update autoclickers set fyke={fk.Owned}",
                $"update autoclickers set rowboat={rb.Owned}",
                $"update autoclickers set cutter={cr.Owned}"
            };


            //loops through the jagged array to execute the commands
            for (int i = 0; i< 3; i++)
            {
                //gets the length of the jagged array for the to execute the code the correct number of times.
                for (int j = 0; j<jaggedSaveCommands[i][j].Length; j++)
                {
                    UpdateTables(jaggedSaveCommands[i][j]);
                }
            }
            */
