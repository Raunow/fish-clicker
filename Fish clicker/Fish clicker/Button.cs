﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Fish_Clicker
{
    class Button : GameObject
    {
        #region Fields
        private enum BState { HOVER, UP, JUST_RELEASED, DOWN }

        //mouse pressed and mouse just pressed
        private bool mpressed, prev_mpressed = false;

        //mouse location in window
        private int mx, my;
        private const int numberOfButtons = 16;

        //colours the button depending on each buttons state
        private Color[] button_color = new Color[numberOfButtons];
        private BState[] button_state = new BState[numberOfButtons];
        private double[] button_timer = new double[numberOfButtons];
        private int[,] button_position = new int[numberOfButtons, 2];

        //saves alle button rectangles and textures in arrays
        private Rectangle[] button_rectangle = new Rectangle[numberOfButtons];
        private Texture2D[] button_texture = new Texture2D[numberOfButtons];
        private Vector2 ownedTextPos, priceTextPos;
        private SpriteFont spriteFont;
        private double frame_time;

        GameWorld gw;
        Fish fh;
        Fishingrod fr;
        Fisherman fm;
        Fyke fk;
        RowBoat rb;
        Cutter cr;
        Table tb;
        #endregion

        #region Constructor
        public Button(GameWorld gw, Fish fh, Fishingrod fr, Fisherman fm, Fyke fk, RowBoat rb, Cutter cr, Table tb)
        {
            this.gw = gw;
            this.fh = fh;
            this.fr = fr;
            this.fm = fm;
            this.fk = fk;
            this.rb = rb;
            this.cr = cr;
            this.tb = tb;
        }
        #endregion 

        #region Methods
        public void Initialize()
        {
            for (int i = 0; i < numberOfButtons; i++)
            {
                if (i == 0)
                {
                    button_position[i, 0] = (125 * (int)gw.Ratio.X);
                    button_position[i, 1] = (365 * (int)gw.Ratio.Y);
                }
                else if (i > 0 && i < 6)
                {
                    button_position[i, 0] = (1503 * (int)gw.Ratio.X);
                    button_position[i, 1] = (156 + ((i - 1) * button_texture[i].Height) * (int)gw.Ratio.Y);
                }
                else if (i > 5 && i < 9)
                {
                    button_position[i, 0] = (1538 + ((i - 6) * (button_texture[i].Width + 70)) * (int)gw.Ratio.X);
                    button_position[i, 1] = (611 * (int)gw.Ratio.Y);
                }
                else if (i > 8 && i < 12)
                {
                    button_position[i, 0] = (1538 + ((i - 9) * (button_texture[i].Width + 70)) * (int)gw.Ratio.X);
                    button_position[i, 1] = (704 * (int)gw.Ratio.Y);
                }
                else if (i > 11 && i < 16)
                {
                    button_position[i, 0] = (1520 * (int)gw.Ratio.X);
                    button_position[i, 1] = (817 + ((i - 12) * (button_texture[i].Height + 15)) * (int)gw.Ratio.Y);
                }

            }

            for (int i = 0; i < numberOfButtons; i++)
            {
                button_state[i] = BState.UP;
                button_color[i] = Color.White;
                button_timer[i] = 0.0;
                button_rectangle[i] = new Rectangle(button_position[i, 0], button_position[i, 1], button_texture[i].Width, button_texture[i].Height);
            }
        }

        public override void LoadContent(ContentManager content)
        {
            button_texture[0] = content.Load<Texture2D>("SmallPond");
            button_texture[1] = content.Load<Texture2D>("RodCounter");
            button_texture[2] = content.Load<Texture2D>("FishermanCounter");
            button_texture[3] = content.Load<Texture2D>("FykeCounter");
            button_texture[4] = content.Load<Texture2D>("RowBoatCounter");
            button_texture[5] = content.Load<Texture2D>("CutterCounter");
            button_texture[6] = content.Load<Texture2D>("FishRedUpgrade");
            button_texture[7] = content.Load<Texture2D>("RodRedUpgrade");
            button_texture[8] = content.Load<Texture2D>("FishermanRedUpgrade");
            button_texture[9] = content.Load<Texture2D>("FykeRedUpgrade");
            button_texture[10] = content.Load<Texture2D>("RowBoatRedUpgrade");
            button_texture[11] = content.Load<Texture2D>("CutterRedUpgrade");
            button_texture[12] = content.Load<Texture2D>("TextSave");
            button_texture[13] = content.Load<Texture2D>("TextLoad");
            button_texture[14] = content.Load<Texture2D>("TextDelete");
            button_texture[15] = content.Load<Texture2D>("TextQuit");

            spriteFont = content.Load<SpriteFont>("spriteFont");
            Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            frame_time = gameTime.ElapsedGameTime.Milliseconds / 1000.0;

            // update mouse variables
            MouseState mouse_state = Mouse.GetState();
            mx = mouse_state.X;
            my = mouse_state.Y;
            prev_mpressed = mpressed;
            mpressed = mouse_state.LeftButton == ButtonState.Pressed;

            update_buttons();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < numberOfButtons; i++)
            {
                spriteBatch.Draw(button_texture[i], button_rectangle[i], button_color[i]);
                
                ownedTextPos.X = (button_rectangle[i].X + 80);
                ownedTextPos.Y = (button_rectangle[i].Y + 25);
                priceTextPos.X = (ownedTextPos.X + 250);
                priceTextPos.Y = ownedTextPos.Y;
                switch (i)
                {
                    case 1:
                        spriteBatch.DrawString(spriteFont, "" + fr.Owned, ownedTextPos, Color.AliceBlue);
                        spriteBatch.DrawString(spriteFont, "" + fr.Price, priceTextPos, Color.AliceBlue);
                        break;

                    case 2:
                        spriteBatch.DrawString(spriteFont, "" + fm.Owned, ownedTextPos, Color.AliceBlue);
                        spriteBatch.DrawString(spriteFont, "" + fm.Price, priceTextPos, Color.AliceBlue);
                        break;

                    case 3:
                        spriteBatch.DrawString(spriteFont, "" + fk.Owned, ownedTextPos, Color.AliceBlue);
                        spriteBatch.DrawString(spriteFont, "" + fk.Price, priceTextPos, Color.AliceBlue);
                        break;

                    case 4:
                        spriteBatch.DrawString(spriteFont, "" + rb.Owned, ownedTextPos, Color.AliceBlue);
                        spriteBatch.DrawString(spriteFont, "" + rb.Price, priceTextPos, Color.AliceBlue);
                        break;

                    case 5:
                        spriteBatch.DrawString(spriteFont, "" + cr.Owned, ownedTextPos, Color.AliceBlue);
                        spriteBatch.DrawString(spriteFont, "" + cr.Price, priceTextPos, Color.AliceBlue);
                        break;

                    default:
                        break;
                }

                

                

            }
        }

        #region HitDetection
        // wrapper for hit_image_alpha taking Rectangle and Texture
        Boolean hit_image_alpha(Rectangle rect, Texture2D tex, int x, int y)
        {
            return hit_image_alpha(0, 0, tex, tex.Width * (x - rect.X) /
                rect.Width, tex.Height * (y - rect.Y) / rect.Height);
        }

        // wraps hit_image then determines if hit a transparent part of image 
        Boolean hit_image_alpha(float tx, float ty, Texture2D tex, int x, int y)
        {
            if (hit_image(tx, ty, tex, x, y))
            {
                uint[] data = new uint[tex.Width * tex.Height];
                tex.GetData<uint>(data);
                if ((x - (int)tx) + (y - (int)ty) *
                    tex.Width < tex.Width * tex.Height)
                {
                    return ((data[
                        (x - (int)tx) + (y - (int)ty) * tex.Width
                        ] &
                                0xFF000000) >> 24) > 20;
                }
            }
            return false;
        }

        // determine if x,y is within rectangle formed by texture located at tx,ty
        Boolean hit_image(float tx, float ty, Texture2D tex, int x, int y)
        {
            return (x >= tx &&
                x <= tx + tex.Width &&
                y >= ty &&
                y <= ty + tex.Height);
        }
        #endregion

        // determine button state(Hover, pressed....) and color of button
        public void update_buttons()
        {
            for (int i = 0; i < numberOfButtons; i++)
            {
                //if (hit_image_alpha(button_rectangle[i], button_texture[i], mx, my))
                if (hit_image_alpha(button_rectangle[i], button_texture[i], mx, my))
                {
                    button_timer[i] = 0.0;
                    if (mpressed)
                    {
                        // mouse is currently down
                        button_state[i] = BState.DOWN;
                        button_color[i] = Color.Gray;
                    }
                    else if (!mpressed && prev_mpressed)
                    {
                        // mouse was just released
                        if (button_state[i] == BState.DOWN)
                        {
                            // button i was just down
                            button_state[i] = BState.JUST_RELEASED;
                        }
                    }
                    else
                    {
                        button_state[i] = BState.HOVER;
                        button_color[i] = Color.LightGray;
                    }
                }
                else
                {
                    button_state[i] = BState.UP;
                    if (button_timer[i] > 0)
                    {
                        button_timer[i] = button_timer[i] - frame_time;
                    }
                    else
                    {
                        button_color[i] = Color.White;
                    }
                }

                if (button_state[i] == BState.JUST_RELEASED)
                {
                    take_action_on_button(i);
                }
            }
        }

        // Logic for each button click goes here
        public void take_action_on_button(int i)
        {
            //take action corresponding to which button was clicked
            switch (i)
            {
                case 0:
                    gw.FishCount += 1 * (int)Math.Pow(1, (double)fh.Level);
                    gw.TotalFish += 1 * (int)Math.Pow(1, (double)fh.Level);
                    gw.TotalClicks++;
                    break;

                case 1:
                    fr.Buying = true;
                    break;

                case 2:
                    fm.Buying = true;
                    break;

                case 3:
                    fk.Buying = true;
                    break;

                case 4:
                    rb.Buying = true;
                    break;

                case 5:
                    cr.Buying = true;
                    break;

                case 6:
                    fh.Upgrading = true;
                    break;

                case 7:
                    fr.Upgrading = true;
                    break;

                case 8:
                    fm.Upgrading = true;
                    break;

                case 9:
                    fk.Upgrading = true;
                    break;

                case 10:
                    rb.Upgrading = true;
                    break;

                case 11:
                    cr.Upgrading = true;
                    break;

                case 12:
                    tb.Save();
                    break;

                case 13:
                    tb.Load();
                    break;

                case 14:
                    tb.Delete();
                    break;

                case 15:
                    gw.GameExit();
                    break;

                default:
                    break;
            }
        }
        #endregion
    }
}